The Wood Norton offers fifty rooms including five spectacular suites, a restaurant using the finest local produce, contemporary bar, alfresco dining terrace, sumptuous private dining, state-of-the-art boardroom, meeting facilities, extensive grounds, formal gardens, and a beautiful wedding venue.

Address: Worcester Road, Evesham, Worcestershire WR11 4YB, UK

Phone: +44 1386 765611